package com.apress.todo.service;

import com.apress.todo.domain.ToDo;
import com.apress.todo.repository.CommonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Class ToDoServiceImpl
 * todo: описание
 *
 * @author Kaleganov Alexander
 * @since 05 янв. 21
 */
@Service
public class ToDoServiceImpl implements ToDoService<ToDo> {
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public ToDo save(ToDo domain) {
        return this.commonRepository.save(domain);
    }

    @Override
    public List<ToDo> save(Collection<ToDo> domains) {
        return this.commonRepository.saveAll(domains);
    }


    @Override
    public void delete(Long id) {
        this.commonRepository.deleteById(id);

    }

    @Override
    public ToDo findById(Long id) {
        return this.commonRepository.findById(id).orElse(new ToDo());
    }

    @Override
    public List<ToDo> findAll() {
        return this.commonRepository.findAll();
    }


}
