package com.apress.todo.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
/**
 * Class ToDo
 * объект списка
 *
 * @author Kaleganov Alexander
 * @since 26 дек. 20
 */
@Data
@Entity
@NoArgsConstructor
@ApiModel(
        value = "ToDo",
        description = "тестовый объект todo"
)
public class ToDo {
    public ToDo(@NonNull @NotBlank String description) {
        this.description = description;
    }

    @ApiModelProperty(value = "Идентификатор")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Описание")
    @NonNull
    @NotBlank
    private String description;

    @ApiModelProperty(value = "Дата и время создания")
    @Column(updatable = false)
    @CreatedDate
    private LocalDateTime created;

    @ApiModelProperty(value = "Дата и время изменения")
    @LastModifiedDate
    private LocalDateTime modified;

    @ApiModelProperty(value = "Boolean значение")
    private boolean completed;

}
