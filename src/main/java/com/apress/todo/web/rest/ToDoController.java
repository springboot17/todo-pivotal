package com.apress.todo.web.rest;

import com.apress.todo.domain.ToDo;
import com.apress.todo.service.ToDoService;
import com.apress.todo.validator.ToDoValidationError;
import com.apress.todo.validator.ToDoValidationErrorBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * Class ToDoController
 * todo-контроллер
 *
 * @author Kaleganov Alexander
 * @since 26 дек. 20
 */
@AllArgsConstructor
@RestController
@RequestMapping("/api")
@Api(tags = "ToDo")
public class ToDoController {
    private final ToDoService<ToDo> toDoToDoService;

    @ApiOperation(value = "Получение списка todo", nickname = "/todo")
    @GetMapping("/todo")
    public ResponseEntity<Iterable<ToDo>> getToDos() {
        return ResponseEntity.ok(toDoToDoService.findAll());
    }

    @ApiOperation(value = "Получение  todo по id", nickname = "/todo/{id}")
    @GetMapping("/todo/{id}")
    public ResponseEntity<ToDo> getToDoById(@PathVariable Long id) {
        return ResponseEntity.ok(toDoToDoService.findById(id));
    }

    @ApiOperation(value = "Обновить / добавить  todo по id", nickname = "/todo")
    @RequestMapping(value = "/todo", method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<?> createToDo(@Valid @RequestBody ToDo toDo, Errors errors) {
        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ToDoValidationErrorBuilder.fromBindingErrors(errors));
        }

        ToDo result = toDoToDoService.save(toDo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Удалить  todo по id", nickname = "/todo/{id}")
    @DeleteMapping("/todo/{id}")
    public ResponseEntity<ToDo> deleteToDo(@PathVariable Long id) {
        toDoToDoService.delete(id);
        return ResponseEntity.noContent().build();
    }


    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ToDoValidationError handleException(Exception exception) {
        return new ToDoValidationError(exception.getMessage());
    }
}
