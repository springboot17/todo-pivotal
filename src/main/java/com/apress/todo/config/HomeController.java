package com.apress.todo.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Home redirection to swagger api documentation.
 */
@Controller
public class HomeController {

    /**
     * Переадресовывает на страницу с документацией.
     *
     * @return Переадресация.
     */
    @GetMapping(value = "/")
    public String getIndex() {
        return "redirect:swagger-ui/";
    }

}
